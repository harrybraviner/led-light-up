; Assembler code for a pic 18f2420
; Should set all of the bits of port A to
; a 'high' output value
; I can examine them using LEDs
INCLUDE "p18f2420.inc"

  CONFIG	OSC=INTIO67	; configure the PIC to use the internal oscillator, with general I/O on pins RA6 and RA7

  org	0
  goto	main
  org	0x0200

main
  clrf	PORTA
  clrf	TRISA	; clear the TRISA registers to set all RA pins to digital output
  movlw	0xff	; value to set the PORTA pins to
  movwf	LATA	; write to PORTA

here
  goto	here	; loop forever
  end
